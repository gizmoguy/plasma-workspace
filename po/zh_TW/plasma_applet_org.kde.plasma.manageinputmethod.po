# Chinese translations for plasma-workspace package
# plasma-workspace 套件的正體中文翻譯.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-13 00:47+0000\n"
"PO-Revision-Date: 2022-03-09 00:47+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: contents/ui/manage-inputmethod.qml:64
#, kde-format
msgctxt "Opens the system settings module"
msgid "Configure Virtual Keyboards..."
msgstr ""

#: contents/ui/manage-inputmethod.qml:79
#, kde-format
msgid "Virtual Keyboard: unavailable"
msgstr ""

#: contents/ui/manage-inputmethod.qml:90
#, kde-format
msgid "Virtual Keyboard: disabled"
msgstr ""

#: contents/ui/manage-inputmethod.qml:104
#, kde-format
msgid "Show Virtual Keyboard"
msgstr ""

#: contents/ui/manage-inputmethod.qml:115
#, kde-format
msgid "Virtual Keyboard: visible"
msgstr ""

#: contents/ui/manage-inputmethod.qml:128
#, kde-format
msgid "Virtual Keyboard: enabled"
msgstr ""
